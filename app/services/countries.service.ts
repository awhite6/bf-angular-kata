import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { concatAll, map, take, tap, toArray } from 'rxjs/operators';
import { DestinationCountry } from '../models/destination-country';
import { HostCountry } from '../models/host-country';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class CountriesService extends BaseService {

  destinationCountryUrl: string = this.baseUrl + "/destination_country"
  hostCountryUrl: string = this.baseUrl + "/host_country"

  // this way you don't have to make the call every time you visit the page? idk this might be dumb
  destinationCountries: Array<DestinationCountry> = []
  hostCountries: Array<HostCountry> = []

  constructor(private http: HttpClient) { 
    super()
  }

  getDestinationCountries(): Observable<Array<DestinationCountry>> {
    return this.http.get(this.destinationCountryUrl, { headers: this.baseHeaders }).pipe(
      toArray(),
      concatAll(),
      map((result:any) => {
        return result.map((country: any) => {
          return new DestinationCountry(country['name'], country['country_id'])
        })
      })
    )
  }

  getHostCountries(): Observable<Array<HostCountry>> {
    return this.http.get(this.hostCountryUrl, { headers: this.baseHeaders }).pipe(
      toArray(),
      concatAll(),
      map((result:any) => {
        return result.map((country: any) => {
          return new HostCountry(country['name'], country['country_id'], country['host_country_states'])
        })
      })
    )
  }
}

