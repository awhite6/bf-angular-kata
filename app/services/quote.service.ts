import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Quote } from '../models/quote';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class QuoteService extends BaseService {
  constructor(private http: HttpClient) {
    super()
   }

   getNewQuote(params: Object): Observable<Quote> {
    return this.http.post("https://coding-challenge-api.bfdevsite.com/api/v1/token/quotation", params, { headers: this.baseHeaders }).pipe(
      map((result: any) => {
        console.log(result)
        return new Quote(result['total'], result['premium'], result['tax'], result['fees'], result['currency_id'], result['return_url'], result['qutation_id']);
      })
    );
  }
}
