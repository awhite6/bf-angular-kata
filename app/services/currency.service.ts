import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { concatAll, map, toArray } from 'rxjs/operators';
import { Currency } from '../models/currency';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class CurrencyService extends BaseService {

  allowedCurrencyUrl: string = "https://coding-challenge-api.bfdevsite.com/api/v1/token/product/8/country/"

  constructor(private http: HttpClient) {
    super();
  }

  getAllowedCurrency(countryId: string): Observable<Array<Currency>> {
    const completeCurrencyUrl = this.allowedCurrencyUrl + countryId + "/currencies"

    return this.http.get(completeCurrencyUrl, { headers: this.baseHeaders}).pipe(
      toArray(),
      concatAll(),
      map((result:any) => {
        return result.map((currency: any) => {
          return new Currency(currency['name'], currency['iso_code'])
        })
      })
    )  
  }
}
