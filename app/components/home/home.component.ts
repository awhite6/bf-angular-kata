import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  showForm: boolean = true

  constructor() { }

  ngOnInit(): void {
  }

  doesQuoteHaveData(): boolean {
    let hasData
    sessionStorage.getItem("quote") != null ? hasData =  true : hasData = false
    return hasData
  }

}
