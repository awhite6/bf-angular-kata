import { Component, Host, OnInit, ɵCompiler_compileModuleSync__POST_R3__ } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CountriesService } from 'src/app/services/countries.service';
import { CurrencyService } from 'src/app/services/currency.service';
import { HostCountry } from 'src/app/models/host-country';
import { DestinationCountry } from 'src/app/models/destination-country';
import { QuoteService } from 'src/app/services/quote.service';
import { Currency } from 'src/app/models/currency';
import { Quote } from 'src/app/models/quote';

@Component({
  selector: 'app-quote-form',
  templateUrl: './quote-form.component.html',
  styleUrls: ['./quote-form.component.css']
})
export class QuoteFormComponent implements OnInit {

  quoteForm = new FormGroup({
    product_id: new FormControl(8),
    host_country: new FormControl('', [Validators.required]),
    country_state: new FormControl(''),
    age: new FormControl('', [Validators.required]),
    currency_id: new FormControl('', [Validators.required]),
    destination_country_ids: new FormControl('', [Validators.required]),
    trip_cost: new FormControl('', [Validators.required]),
    start_date: new FormControl('', [Validators.required]),
    end_date: new FormControl('', [Validators.required]),
    winter_sports_extension: new FormControl(false),
    deposit_date: new FormControl('', [Validators.required])
  })

  hostCountries: Array<HostCountry> = []
  countryStates: Array<string> = []
  destinationCountries: Array<DestinationCountry> = []
  allowedCurrencies: Array<Currency> = []
  quote: Quote | undefined
  ages: number[] = Array.from(Array(100).keys())

  constructor(private countryService: CountriesService, 
              private currenyService: CurrencyService,
              private quoteService: QuoteService) {}

  ngOnInit(): void {
    this.countryService.getDestinationCountries().subscribe(result => this.destinationCountries = result)
    this.countryService.getHostCountries().subscribe(result => this.hostCountries = result)
  }

  isAgeDisabled(age: number): boolean {
    return this.quoteForm.value.age.length < 1 && age < 18
  }

  hostCountryChanged(): void {
    const hostCountry = this.quoteForm.value.host_country
    this.setAllowedCurrencies(hostCountry.iso_code)
    this.setCountryStates(hostCountry.country_states)
  }

  setAllowedCurrencies(event: any): void {
    this.currenyService.getAllowedCurrency(event.value).subscribe(result => this.allowedCurrencies = result)
  }

  private setCountryStates(countryStates: Array<any>): void {
    this.countryStates = countryStates.map(state => {
      return state['country_state']['code']
    })
  }

  private createPayload(): Object {
    const formValues = this.quoteForm.value
    const destinations = formValues.destination_country_ids.join(',')
    const ages = formValues.age.join(',')

    return {
      "product_id": 8,
      "age": ages,
      "currency_id": formValues.currency_id,
      "destination_country_ids": destinations,
      "host_country_id": formValues.host_country.iso_code,
      "country_state": formValues.country_state,
      "start_date": formValues.start_date,
      "end_date": formValues.end_date,
      "trip_cost": formValues.trip_cost,
      "deposit_date": formValues.deposit_date,
      "winter_sports_extension": formValues.winter_sports_extension
    }
  }

  onSubmit(): void {
    this.quoteService.getNewQuote(this.createPayload()).subscribe((result: Quote) => {
      console.log(result)
      sessionStorage.setItem("quote", JSON.stringify(result))
    })
  }
}
