import { Component, Input, OnInit } from '@angular/core';
import { Quote } from 'src/app/models/quote';
import { QuoteService } from 'src/app/services/quote.service';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.css']
})
export class QuoteComponent implements OnInit {
  data: Quote = new Quote(0,0,0,0,'','',0)

  constructor() {
  }

  ngOnInit(): void {
    const storedQuote: string = sessionStorage.getItem('quote') || ''
    this.data = JSON.parse(storedQuote) as Quote
  }

  deleteQuote(): void {
    sessionStorage.removeItem('quote')
  }

}
