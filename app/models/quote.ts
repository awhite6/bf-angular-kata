export class Quote {
    constructor(
        public total: number,
        public premium: number,
        public tax: number,
        public fees: number,
        public currency_id: string,
        public return_url: string,
        public quotation_id: number
    ) {}
}
