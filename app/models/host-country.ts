export class HostCountry {
    constructor(
        public name: string,
        public iso_code: string,
        public country_states: Array<any>
    ) {}
}
