export class DestinationCountry {
    constructor(
        public name: string,
        public iso_code: string,
    ) {}
}
