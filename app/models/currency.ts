export class Currency {
    constructor(
        public name: string,
        public iso_code: string,
    ) {}
}
