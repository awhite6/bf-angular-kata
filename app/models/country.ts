export class Country {
    constructor(
        public name: string,
        public iso_code: string,
    ) {}
}
